Java JDK Dockerfile
=================

Short description
-----------------
The Java Development Kit (JDK) is an implementation of either one of the Java Platform, Standard Edition.
The JDK includes a private JVM and a few other resources to finish the development of a Java Application.

How to build image
------------------
List the available branches in this repository to find your JDK version :
```
git clone ${repository}
cd ${repository}
git branch -alvv
git checkout ${your_jdk_version}
docker build -t ${docker_image_name}:${docker_tag_name} .
```

ENJOY ;)
